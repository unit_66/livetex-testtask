$( document ).ready(function() {
    var noteStorage = localStorage;
    window.addEventListener('storage', storage_handler, false);

    renderNote = function () {
        $('.render-area').html('');
        lastId  =  0;
        $.each(noteStorage, function (i) {
            retrievedNote = noteStorage.getItem(i);
            id      =   JSON.parse(retrievedNote).id;
            title   =   JSON.parse(retrievedNote).title;
            text    =   JSON.parse(retrievedNote).text;
            color   =   'style="background-color: ' + JSON.parse(retrievedNote).color + '"';
            lastId  =   id;
            $('.render-area').append('<section class="columns large-3 medium-4 small-6"><article id="note' + id + '" class="note" ' +  color + '><a class="fa fa-pencil editNote" aria-hidden="true"></a><a class="fa fa-check editNote editNoteOk" aria-hidden="true" style="display: none"></a><a class="fa fa-times-circle delNote" aria-hidden="true"></a><h4>' + title + '</h4><input type="text" id="editTitle" style="display: none;" placeholder="' + title + '"><p>' + text + '</p><textarea id="editText" style="display: none;" placeholder="' + text + '"></textarea></article></section>');
        });
        $('.render-area').append('<section class="columns large-3 medium-4 small-6"><article class="note newNote"><div class="plus"><a class="add">+</a></div><div class="addForm" style="display: none"><h4><input id="newTitle" type="text" placeholder="Новая заметка"></input></h4><p><textarea id="newText" placeholder="Ваш текст здесь"></textarea></p><a id="addNewNote" class="button">Добавить</a></div></article></section>');
        $('.add').click(function(){
            $(this).hide();
            $('.addForm').fadeIn();
            $('#addNewNote').click(function(){
                var newId = lastId + 1;
                function get_random_color() {
                    return "#"+((1<<24)*Math.random()|0).toString(16);
                };
                var randomColor = get_random_color();
                var newNote         = {
                    id    : newId,
                    title : $('#newTitle').val(),
                    text  : $('#newText').val(),
                    color : randomColor
                };
                noteStorage.setItem('note' + newId , JSON.stringify(newNote));
                renderNote();
            });
        });
        $('.delNote').click(function(){
            var noteForDel = $(this).parent().attr('id');
            noteStorage.removeItem(noteForDel);
            renderNote();
        });
        $('.editNote').click(function(){
            $(this).siblings('h4').hide();
            $(this).siblings('p').hide();
            $(this).hide();
            $(this).siblings('#editTitle').fadeIn();
            $(this).siblings('#editText').fadeIn();
            $(this).siblings('a.editNoteOk').show();
            $(this).siblings('a.editNoteOk').click(function () {
                var  noteForEdit = $(this).parent().attr('id');
                var oldValues = noteStorage.getItem(noteForEdit);
                var oldId      =   JSON.parse(oldValues).id;
                var oldColor   =   JSON.parse(oldValues).color;
                var editedNote         = {
                         id    : oldId,
                         title : $(this).siblings('#editTitle').val(),
                         text  : $(this).siblings('#editText').val(),
                         color : oldColor
                };
                console.log(editedNote);
                noteStorage.setItem(noteForEdit , JSON.stringify(editedNote));
                renderNote();
            });
        });
    };

    renderNote();

    function storage_handler(){
        renderNote();
    };

    $('#clear').click(function(){
        noteStorage.clear();
        lastId  =  0;
        renderNote();
    });
});